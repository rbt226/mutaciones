const Sequelize = require("sequelize");
const db = require("../db.js");

const Model = Sequelize.Model;
class Mutation extends Model {}

// allowNull defaults to true

Mutation.init(
  {
    // attributes
    dna: {
      type: Sequelize.STRING,
      primaryKey: true,
    },
    has_mutation: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
    },
  },
  {
    sequelize: db,
    modelName: "mutations",
    schema: "public",

    // options
  }
);
module.exports = Mutation;
