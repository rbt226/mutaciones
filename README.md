# Mutaciones

Para correr el proyecto de manera local:
1- npm install
2- npm start

# Test Automaticos:

Installar de forma global:
1- mocha: npm install mocha -g
2- nyc: npm install nyc -g

Ubicarse en la raiz del proyecto y ejecutar el siguiente comando:
nyc --reporter=html --reporter=text mocha --timeout 3000

Se puede observar el test coverage en la consola o se puede abrir en un browser el html generado dentro de la carpeta coverage:
-> ./coverage/index.html:

9 passing (5s)

-------------------|---------|----------|---------|---------|-------------------
File               | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s 
-------------------|---------|----------|---------|---------|-------------------
All files          |   93.13 |    86.84 |      80 |   92.86 |                   
 mutaciones        |   95.24 |      100 |   66.67 |   95.24 |                   
  app.js           |     100 |      100 |     100 |     100 |                   
  db.js            |   85.71 |      100 |      50 |   85.71 | 29                
 mutaciones/model  |     100 |      100 |     100 |     100 |                   
  ...tion.model.js |     100 |      100 |     100 |     100 |                   
 ...iones/services |   92.38 |    86.11 |   82.35 |      92 |                   
  mutation.js      |   92.47 |     87.5 |   84.62 |   92.13 | ...8-49,54,76,162 
  stats.js         |   91.67 |       75 |      75 |   90.91 | 27                
-------------------|---------|----------|---------|---------|-------------------

# ACLARACION:

Para asegurarse que el coverage de los test de mayor a 80% deberan ejecutarse antes de realizar pruebas manuales contra la API REST
esto se debe a que ninguna de las secuencias de ADN utilizadas en los test deben existir en la base de datos.

Dado que solamente debe existir un único registro por cada secuencia de ADN, se opto como solucion validar la existencia para cada peticion
y en caso de que la secuencia ya estuviese almacenada no se vuelve a ejecutar el algoritmo sino que se devuelve el valor almacenado con el
fin de mejorar la eficiencia.
También se agrega un control para que la tabla generada a partir de la secuencia de ADN cumpla con el formato (NxN) debiendo tener la misma cantidad de valores tanto las filas como las columnas.
Además se valida que la secuencia no contenga valores de bases nitrogenadas que sean correctos (A, T, C, G) y de no cumplir con dicha condición se retorna un error y no se continúa evaluando la secuencia.

# Hosteo API REST

La API REST creada se hosteo en el cloud computing Heroku. Se puede acceder a los servicios mediante los siguientes enlaces:
https://prueba-candidato-robert.herokuapp.com/stats -> GET
https://prueba-candidato-robert.herokuapp.com/mutation -> POST

# Ejemplos de peticiones:

Con Mutación: { "dna": ["TTTTAC", "GGGGCA", "AAAAGT", "GTAAGG", "CCTCTA", "TCACTG"] },
Con Mutación: { "dna": ["GTCCAC", "GTGTGC", "GTATGT", "GTAAGG", "CCTCTA", "TCACTG"] },
Con Mutación: { "dna": ["GTCCAC", "AGGTCC", "GTGCGT", "GTCGGG", "CCTCTA", "TCACTG"] },
Sin Mutación: { "dna": ["GTCCAC", "ACGTGC", "GTATGT", "GTATGG", "CCTCTA", "TCACTG"] },


# Base de datos

La base de datos para guardar los ADN verificados tambien se creo en Heroku, las credenciales para acceder a la misma son:

-> Host: ec2-3-215-83-17.compute-1.amazonaws.com
-> Database: d4gp0b01iogj
-> User: dpvcphbhmefflm
-> Port: 5432
-> Password: 1a9deb8bd6220d795381c375c6b6a96c668d3b9c0f590b02134bc005c21a2974
