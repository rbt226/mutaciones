const mutationModel = require("../model/mutation.model");

exports.getStats = (req, res) => {
  var count_mutations = 0;
  var count_no_mutation = 0;
  mutationModel
    .findAll() // Se obtienen todos los ADN almacenados en la base
    .then((mutations) => {
      mutations.forEach((element) => {
        // Para cada uno de los ADN obtenidos se contabiliza cuales tienen mutacion y cuales no
        if (element.has_mutation) count_mutations++;
        else count_no_mutation++;
      });
      var multiplier = 100; // Se redondea a 2 decimales

      return res.json({
        count_mutations,
        count_no_mutation,
        ratio:
          count_no_mutation == 0
            ? 0
            : Math.round(count_mutations / count_no_mutation * multiplier) /
              multiplier, // Se calcula el ratio
      });
    })
    .catch((error) => {
      return res.status(403).send("Error al obtener todas las mutaciones");
    });
};
