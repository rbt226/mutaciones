const mutationModel = require("../model/mutation.model");

var baseAnterior = "";
var countLetrasIguales = 1;
var tablaADN = [];
var countSecuencialetrasIguales = 0;
var dnaLength = 0;

exports.hasMutation = (req, res) => {
  dna = req.body.dna;
  dnaLength = dna.length;
  countSecuencialetrasIguales = 0;
  tablaADN = [];
  existMutation(dna).then((existMutation) => {
    switch (existMutation) {
      case true:
        return res.status(200).send("Con mutación");
      case false:
        return res.status(403).send("Sin mutación");
      default:
        try {
          if (compareRows(dna) || compareColumns() || compareDiagonal()) {
            saveMutation(dna, true);
            return res.status(200).send("Con mutación");
          }
        } catch (error) {
          return res.status(403).send(error);
        }
        saveMutation(dna, false);
        return res.status(403).send("Sin mutación");
    }
  });
};

/* existMutation:
    -> null:  no existe un registro en la base de datos para ese ADN
    -> true:  existe un registro en la base de datos con mutacion 
    -> false:  existe un registro en la base de datos sin mutacion 

    En caso de que el ADN se encuentre en la base de datos se retorna si esa secuencia 
    contiene mutación o no, evitando asi evaluarla nuevamente
*/

existMutation = (dna) => {
  return new Promise((resolve) => {
    mutationModel
      .findOne({ where: { dna: dna.toString() } })
      .then((mutation) => {
        if (mutation) {
          resolve(mutation.dataValues.has_mutation);
          return;
        }
        resolve(null);
      })
      .catch((error) => {
        resolve(null);
      });

    // Validate email, username and document?
  });
};

/* saveMutation:
    Crea el registro de ADN en la base de datos, unicamente si el mismo no existe
*/

saveMutation = (dna, isMutated) => {
  const adn = {
    dna: dna.toString(),
    has_mutation: isMutated,
  };
  mutationModel
    .create(adn)
    .then(() => {
      console.log("Se ha creado exitosamente");
    })
    .catch((error) => {
      console.log("Error al crear registro en la base: ", error);
    });
};

/* compareRows:
    Valida si existe una mutacion para cada fila de la secuencia del ADN, si encuentra una mutacion se hace un retorno temprano
    Recorre la tabla de ADN segun el eje X
*/

compareRows = (dna) => {
  const validBase = ["A", "T", "C", "G"];
  var secuenciaLenth = 0;
  for (var i = 0; i < dnaLength; i++) {
    secuencia = dna[i];
    secuenciaLenth = secuencia.split("").length;
    if (secuenciaLenth !== dnaLength) {
      throw "No es una secuencia de ADN válida"; // Se valida que el ADN sea de NxN, de lo contrario no es una secuencia válida
    }

    baseAnterior = "";
    countLetrasIguales = 1;
    tablaADN[i] = []; // Se crea una matriz para la tabla de las secuencias de ADN

    for (var j = 0; j < secuenciaLenth; j++) {
      base = secuencia[j];

      if (!validBase.includes(base)) {
        throw "No es una base nitrogenada válida"; // Se valida que la base nitrogenada sea: A,T,C o G, de lo contrario no es una base válida
      }

      tablaADN[i][j] = base;
      if (compareDNA(base)) {
        return true;
      }
    }
  }
  return false;
};

/* compareRows:
   Valida si existe una mutacion para cada columna de la secuencia del ADN, si encuentra una mutacion se hace un retorno temprano
   Recorre la tabla de ADN segun el eje Y
*/

compareColumns = () => {
  for (var j = 0; j < dnaLength; j++) {
    baseAnterior = "";
    countLetrasIguales = 1;
    for (var i = 0; i < dnaLength; i++) {
      if (compareDNA(tablaADN[i][j])) {
        return true;
      }
    }
  }
};

/* compareDiagonal:
    Valida si existe una mutacion para cada diagonal de la secuencia del ADN, si encuentra una mutacion se hace un retorno temprano
    Recorre en primera instancia la tabla de ADN de forma diagonal comenzando por el vertice inferior izquierdo avanzando uno a uno 
    por la diagonal para terminar en el vertice superior derecho.
    En caso de no encontrar mutacion, se recorre la diagonal faltante de la tabla (desde el vertice superior izquierda hasta el vertice
    inferior derecho)

*/

compareDiagonal = () => {
  baseAnterior = "";
  countLetrasIguales = 1;
  const length = tablaADN[0].length;
  var i = length - 1;
  var j = 0;

  while (i >= 0 && j < length) {
    if (compareDNA(tablaADN[i][j])) {
      return true;
    }
    i--;
    j++;
  }

  var j = 0;
  var i = 0;
  baseAnterior = "";
  countLetrasIguales = 1;
  while (j >= 0 && i < length) {
    if (compareDNA(tablaADN[i][j])) {
      return true;
    }
    i++;
    j++;
  }
};

/* compareDNA:
  Compara la base nitrogenada de la posicion actual (baseActual) con su anterior (baseAnterior) en cada una de las formas (horizontal, vertical o diagonal),
  En caso de que la base sea igual a la anterior se suma uno al contador de cantidad de letras iguales(countLetrasIguales), al llegar a 4 se suma 1 al 
  contador de secuencia de cuatro letras iguales(countSecuencialetrasIguales) en cambio si son diferentes se resetear el contador countLetrasIguales*/

compareDNA = (baseActual) => {
  if (baseAnterior === baseActual) {
    countLetrasIguales = countLetrasIguales + 1;
  } else {
    baseAnterior = baseActual;
    countLetrasIguales = 1;
  }
  if (countLetrasIguales === 4) {
    countSecuencialetrasIguales++;
    if (countSecuencialetrasIguales > 1) {
      countSecuencialetrasIguales = 0;
      return true;
    }
  }
  return false;
};
