"use strict";

var request = require("supertest");
var app = require("../app");
var supertest = request(app);
var assert = require("assert");

var request = request(
  "http://localhost:https://prueba-candidato-robert.herokuapp.com"
);

describe("ADN", function () {
  describe("POST", function () {
    it("Debe retornar que si tiene mutacion en la fila", function (done) {
      let dna = {
        dna: ["TTTTAC", "GGGGCA", "AAAAGT", "GTAAGG", "CCTCTA", "TCACTG"],
      };

      supertest
        .post("/mutation")
        .send(dna)
        .expect(200)
        .set("Accept", "application/json")
        .expect("Content-Type", /html/)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          assert.equal(res.text.trim(), "Con mutación");
          done();
        });
    });
  });
  describe("POST", function () {
    it("Debe retornar que si tiene mutacion - sin insertar en la base de datos", function (done) {
      let dna = {
        dna: ["TTTTAC", "GGGGCA", "AAAAGT", "GTAAGG", "CCTCTA", "TCACTG"],
      };

      supertest
        .post("/mutation")
        .send(dna)
        .expect(200)
        .set("Accept", "application/json")
        .expect("Content-Type", /html/)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          assert.equal(res.text.trim(), "Con mutación");
          done();
        });
    });
  });
  describe("POST", function () {
    it("Debe retornar que si tiene mutacion en columns", function (done) {
      let dna = {
        dna: ["GTCCAC", "GTGTGC", "GTATGT", "GTAAGG", "CCTCTA", "TCACTG"],
      };

      supertest
        .post("/mutation")
        .send(dna)
        .expect(200)
        .set("Accept", "application/json")
        .expect("Content-Type", /html/)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          assert.equal(res.text.trim(), "Con mutación");
          done();
        });
    });
  });
  describe("POST", function () {
    it("Debe retornar que si tiene mutacion en la diagonal", function (done) {
      let dna = {
        dna: ["GTCCAC", "AGGTCC", "GTGCGT", "GTCGGG", "CCTCTA", "TCACTG"],
      };

      supertest
        .post("/mutation")
        .send(dna)
        .expect(200)
        .set("Accept", "application/json")
        .expect("Content-Type", /html/)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          assert.equal(res.text.trim(), "Con mutación");
          done();
        });
    });
  });
  describe("POST", function () {
    it("Debe retornar que no tiene mutacion", function (done) {
      let dna = {
        dna: ["GTCCAC", "ACGTGC", "GTATGT", "GTATGG", "CCTCTA", "TCACTG"],
      };

      supertest
        .post("/mutation")
        .send(dna)
        .expect(403)
        .set("Accept", "application/json")
        .expect("Content-Type", /html/)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          assert.equal(res.text.trim(), "Sin mutación");
          done();
        });
    });
  });
  describe("POST", function () {
    it("Debe retornar que no tiene mutacion - sin insertar en la base de datos", function (done) {
      let dna = {
        dna: ["GTCCAC", "ACGTGC", "GTATGT", "GTATGG", "CCTCTA", "TCACTG"],
      };

      supertest
        .post("/mutation")
        .send(dna)
        .expect(403)
        .set("Accept", "application/json")
        .expect("Content-Type", /html/)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          assert.equal(res.text.trim(), "Sin mutación");
          done();
        });
    });
  });
  describe("POST", function () {
    it("Debe retornar que no es una secuencia valida", function (done) {
      let dna = {
        dna: ["AGTCCAC", "GTGTGC", "GTATGT", "GTAAGG", "CCTCTA", "TCACTG"],
      };

      supertest
        .post("/mutation")
        .send(dna)
        .expect(403)
        .set("Accept", "application/json")
        .expect("Content-Type", /html/)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          assert.equal(res.text.trim(), "No es una secuencia de ADN válida");
          done();
        });
    });
  });
  describe("POST", function () {
    it("Debe retornar que no es una base valida", function (done) {
      let dna = {
        dna: ["GTCCAC", "GTGTGC", "GTATGT", "GTAAGG", "CCTCPA", "TCACTG"],
      };

      supertest
        .post("/mutation")
        .send(dna)
        .expect(403)
        .set("Accept", "application/json")
        .expect("Content-Type", /html/)
        .end(function (err, res) {
          if (err) {
            return done(err);
          }
          assert.equal(res.text.trim(), "No es una base nitrogenada válida");
          done();
        });
    });
  });
  describe("GET", function () {
    it("Debe retornar una estadistica", function (done) {
      supertest
        .get("/stats")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .expect(200, done);
    });
  });
});
