var express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

var app = express();
const mutation = require("./services/mutation");
const stats = require("./services/stats");
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(cors());

app.post("/mutation", mutation.hasMutation);
app.get("/stats", stats.getStats);

app.listen(port, function () {
  console.log("Example app listening on port: ", port);
});

module.exports = app;
